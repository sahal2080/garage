# Garage

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/chores`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

In `Gemfile` add
```ruby
gem 'garage'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install chores

## Usage

TODO: Write usage instructions here

## Development

1. Fork it.
2. Dig in!

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/chores. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

