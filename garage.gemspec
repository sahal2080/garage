# -*- encoding: utf-8 -*-
# require_relative 'lib/garage.rb'
require_relative 'lib/garage/version.rb'

Gem::Specification.new do |spec|
  spec.name    = "garage"
  spec.version = Garage::VERSION
  spec.author  = 'Sahal Alarabi'
  spec.email   = 'sahal@menuslate.com'

  spec.summary     = %q{Write a short summary, because Rubygems requires one.}
  spec.description = "Organizing utilities under common themes"
  spec.homepage    = ""
  spec.license     = 'MIT'

  spec.platform                  = Gem::Platform::RUBY
  spec.required_ruby_version     = ">= 1.9.0"
  spec.required_rubygems_version = Gem::Requirement.new(">= 0") if spec.respond_to? :required_rubygems_version=

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = "lib/garage.rb"
  spec.require_paths = ['lib']
  # spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  # spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})

  spec.add_dependency "finder"
  spec.add_dependency "rex"

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
end
